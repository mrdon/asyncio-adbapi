#!/usr/bin/env python

import sys
import os
import shutil

from setuptools import setup
from setuptools import Feature
from distutils.cmd import Command
from distutils.command.build_ext import build_ext
from distutils.errors import CCompilerError
from distutils.errors import DistutilsPlatformError, DistutilsExecError
from distutils.core import Extension

requirements = ["asyncio"]

setup(
    name="asyncio-dbapi",
    version="0.1-dev",
    description="Asynchronous Python 3.3+ interface for DB-API 2.0 drivers",
    author="Glyph Lefkowitz and the Twisted Team, Don Brown",
    author_email="mrdon@twdata.org",
    url="https://bitbucket.org/mrdon/asyncio-dbapi",
    keywords=["dbapi", "asyncio"],
    packages=["adbapi"],
    install_requires=requirements,
    features={},
    license="Apache License, Version 2.0",
    test_suite="nose.collector",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: Apache Software License",
        "Operating System :: MacOS :: MacOS X",
        "Operating System :: Microsoft :: Windows",
        "Operating System :: POSIX",
        "Programming Language :: Python",
        "Topic :: Database"],
    cmdclass={"doc": ""})
