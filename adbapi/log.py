"""Logging configuration."""

import logging


# Name the logger after the package.

log = logging.getLogger(__package__)

__all__ = ['log']