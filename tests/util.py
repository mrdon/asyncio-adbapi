from adbapi import ConnectionPool


class NonThreadPool(object):
    def callInThreadWithCallback(self, onResult, f, *a, **kw):
        success = True
        try:
            result = f(*a, **kw)
        except Exception as e:
            success = False
            result = Failure()
        onResult(success, result)



class DummyConnectionPool(ConnectionPool):
    """
    A testable L{ConnectionPool};
    """
    executor = NonThreadPool()

    def __init__(self):
        """
        Don't forward init call.
        """
        self._loop = reactor



class EventReactor(object):
    """
    Partial L{IReactorCore} implementation with simple event-related
    methods.

    @ivar _running: A C{bool} indicating whether the reactor is pretending
        to have been started already or not.

    @ivar triggers: A C{list} of pending system event triggers.
    """
    def __init__(self, running):
        self._running = running
        self.triggers = []


    def callWhenRunning(self, function):
        if self._running:
            function()
        else:
            return self.addSystemEventTrigger('after', 'startup', function)


    def addSystemEventTrigger(self, phase, event, trigger):
        handle = (phase, event, trigger)
        self.triggers.append(handle)
        return handle


    def removeSystemEventTrigger(self, handle):
        self.triggers.remove(handle)
