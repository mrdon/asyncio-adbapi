=======
Asyncio DB-API
=======
:Info: See `Bitbucket <http://bitbucket.org/mrdon/asyncio-dbapi>`_ for the latest source.
:Author: Don Brown<mrdon@twdata.org>

About
=====
An asynchronous Python interface to DB-API 2.0 drivers, based on Python's asyncio.
This project is based on the adbapi module of `Twisted <https://twistedmatrix.com/>`_

This project is still in the very early alpha stage and shouldn't be used for production.

Docs and examples
=================
There are some examples in the *examples/* directory.
